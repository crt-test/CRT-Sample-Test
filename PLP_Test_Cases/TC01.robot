*** Settings ***

Documentation           New test suite
Library                 String
Library                 QWeb
Suite Setup             Open Browser    about:blank    chrome
Test Setup              DeleteAllCookies
Suite Teardown          Close All Browsers

*** Variables ***

*** Test Cases ***

TC01_TH_Verify_PLP_Header

    [Documentation]    Test Case created using the QEditor

    GoTo   https://ee.tommy.com/
    ClickText    Accept All
    ClickText    Women    anchor=Free delivery for orders above €100
    HoverText    Clothing
    ClickText    Jeans   
    VerifyElement   //h2[@data-testid="ProductListHeader-Title"]


                 
